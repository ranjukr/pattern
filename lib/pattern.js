var generateOneLine = function (number) {
    var star = '*';
    return star.repeat(number) + '\n';
};
var Pattern = {};

Pattern.filledRectangle = function (length, width) {
    var result = '';
    for (var j = 0; j < width; j++) {
        result += generateOneLine(length);
    }

    return result;
};

module.exports = Pattern;

const expect = require('chai').expect;

var Pattern = require('../lib/pattern');

describe('filledRectangle', function () {
    it('should give back filled rectangle according to the given size', function () {
        var expected = '****\n****\n****\n';
        var rect = Pattern.filledRectangle(4, 3);
        expect(rect).to.be.equals(expected);
    });

    it('should return empty if length and width are 0', function () {
        var expected = '';
        var rect = Pattern.filledRectangle(0, 0);
        expect(rect).to.be.equals(expected);
    });
});